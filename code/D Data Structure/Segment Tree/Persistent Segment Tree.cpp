ll arr[N];
struct Node {
    int left, right, mid;
    ll sum;
    Node *lc, *rc;
    Node() {left = right = mid = sum = 0, lc = rc = NULL;}
    void pull() {
        sum = lc->sum + rc->sum;
    }
}*root_node[N]; // Store the history root
/* Use new_root = copy_node(old_root)
 * and Update new_root */
Node *copy_node(Node *r) {
    Node* cp = new Node();
    cp->left = r->left; cp->right = r->right;
    cp->mid = r->mid; 
    cp->sum = r->sum;
    cp->lc = r->lc; cp->rc = r->rc;
    return cp;
}
void build(Node *r, int left, int right) {
    r->left = left; r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->sum = arr[left]; 
        return;
    }
    build(r->lc = new Node(), left, r->mid);
    build(r->rc = new Node(), r->mid + 1, right);
    r->pull();
}
void upd(Node *r, Node *new_r, int pos, ll val) {
    if(r->left == r->right) {
        new_r->sum = val;
        return;
    }
    if(pos <= r->mid) {
        new_r->lc = copy_node(r->lc);
        upd(r->lc, new_r->lc, pos, val);
    }
    else {
        new_r->rc = copy_node(r->rc);
        upd(r->rc, new_r->rc, pos, val);
    }
    new_r->pull();
}
ll qry(Node *r, int ql, int qr) {
    if(ql > r->right || qr < r->left) return 0;
    if(ql <= r->left && r->right <= qr) return r->sum;
    return qry(r->lc, ql, qr) + qry(r->rc, ql, qr);
}