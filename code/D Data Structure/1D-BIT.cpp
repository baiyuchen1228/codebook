int n; ll arr[N], pre[N], BIT[N];
void upd(ll x,ll dif) { // update the value at position x by difference dif
    for(int i = x;i <= n;i += (i & -i)) {
        BIT[i] += dif;
    }
}
ll sum(ll x) { // return the sum of values in range [1, x]
    ll res = 0;
    for(int i = x;i > 0;i -= (i & -i)) {
        res = res + BIT[i];
    }
    return res;
}
void build() {
    for(int i = 1;i <= n;i++) { // build, index must start from 1
        int k = i & -i;
        BIT[i] = pre[i] - pre[i - k];
        // pre = prefix sum, index start from 1
    }
}