ll grid[N][N], BIT[N][N]; int n;
ll sum(int x, int y) { // return the rectangle of sum of value from (1, 1) ~ (x, y)
    ll res = 0;
    for(int i = x;i > 0;i -= (i & -i)) {
        for(int j = y;j > 0;j -= (j & -j)) {
            res += BIT[i][j];
        }
    }
    return res;
}
void upd(int x, int y, ll dif) { // update the value of (x, y) by dif  
    for(int i = x;i <= n;i += (i & -i)) {
        for(int j = y;j <= n;j += (j & -j)) {
            BIT[i][j] += dif;
        }
    }
}
void build() {
    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            upd(i, j, grid[i][j]);
        }
    }
}