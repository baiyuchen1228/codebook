"Bounded Knapsack"
for(int i = 0;i < n;i++) { // for each item and Quantity[i] copy
    for(int k = 1;k <= quantity[i];k <<= 1) {
        quantity[i] -= k;
        item.push_back(Item(k * weight[i], k * value[i]));
    }
    if(quantity[i] > 0) item.push_back(Item(quantity[i] * weight[i], quantity[i] * value[i]));
}
// and use 01-Knapsack to solve problem
/* ------------------------------------------ */
"01 and Unbounded Knapsack"
for(int i = 0;i < n;i++) {
    for(int j = x; j - weight[i] >= 0;j--) { // if this is infinite Knapback, j = (weight[i] ~ x)
        dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);
    }
}
ans = dp[x]
/* ------------------------------------------ */
"Sum of Subset"
sum[0] = true;
for(int i = 0;i < n;i++) {
    for(int j = MAX; j >= coin[i];j--) {
        sum[j] |= sum[j - coin[i]];
    }
}
/* ------------------------------------------ */
"LCS"
for(int i = 1;i <= (int)a.size();i++) {
    for(int j = 1;j <= (int)b.size();j++) {
        if(a[i - 1] == b[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
        else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
    }
}
ans = dp[a.size()][b.size()]
/* ------------------------------------------ */
"LIS"
for(int i = 0;i < n;i++) {
    auto it = lower_bound(dp.begin(), dp.end(), arr[i]);
    if(it == dp.end()) dp.push_back(arr[i]);
    else *it = arr[i];
}
ans = dp.size()
/* ------------------------------------------ */
"Removal Game"
ll solve(ll l, ll r) { //calculate left ~ right interval, the first player can get how many score
    if(l > r) return 0;
    if(l == r) return arr[l];
    if(l == r - 1) return max(arr[l], arr[r]);
    if(dp[l][r]) return dp[l][r];
    ll ans1, ans2;
    ans1 = min(arr[l] + solve(l + 2, r),arr[l] + solve(l + 1, r - 1));
    ans2 = min(arr[r] + solve(l + 1, r - 1),arr[r] + solve(l, r - 2));
    return dp[l][r] = max(ans1, ans2);
}
/* ------------------------------------------ */
"Cutting Stick"
int solve(int l, int r) {
    if(r - l <= 1) return 0;
    if(dp[l][r]) return dp[l][r];
    ll ans = INF, len = cut[r] - cut[l]; 
    for(int i = l + 1;i < r;i++) {
        ans = min(ans, len + solve(l, i) + solve(i, r));
    }
    return dp[l][r] = ans;
}
void Cutting_stick() {
    cut[0] = 0, cut[n + 1] = l; // and input cut[1 ~ n]
    ans = solve(0, n + 1);
}