// dp[mask] = sigma of dp[i](i in mask)
for(int k = 0; k < K; k++) { // K = 20
	for(int s = 0; s < (1 << K); s++) {
		if(s & (1 << k)) {
			dp[s] += dp[s ^ (1 << k)];
		}
	}
}