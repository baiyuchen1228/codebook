ll w, a; // selected by random
struct qr {ll r, q}; // r + q * sqrt(w)
qr qr_muti(qr u, qr v, ll p) {
    qr res;
    res.r = (u.r * v.r % p + (u.q * v.q) % p * w) % p;
    res.q = (u.r * v.q % p + u.q * v.r) % p;
    return res;
}
qr qr_fp(qr x, ll y, ll p) {
    qr res;
    res.r = 1, res.q = 0;
    while(y) {
        if(y & 1) res = qr_muti(res, x, p);
        y >>= 1;
        x = qr_muti(x, x, p);
    }
    return res;
}
ll p;
ll Led(ll n) {
    // if return p - 1 unsolveable
    // else if return 1 or 0 sovleable
    return fp(n, (p - 1) / 2, p);
}
ll QC() {
    do {
        a = rand();
        w = a * a - x;
        w = (w % p + p) % p;
    } while (Led(w) != p - 1);

    qr base;
    base.r = a, base.q = 1;
    qr ans = qr_fp(base, (p + 1) / 2, p);
    ans.r = (ans.r % p + p) % p;
    // ans.r is the solution of n^2=x(mod p)
    return ans.r
}