ll Polygon_Area(vector<pair<ll, ll>> &point) {
    ll ans = 0; int n = (int)point.size();
    for(int i = 0;i < n;i++) {
        P p1 = {point[i].X, point[i].Y};
        P p2 = {point[(i + 1) % n].X, point[(i + 1) % n].Y};
        ans += cross(p1, p2);
    }
    return ans > 0 ? ans : -ans;
}