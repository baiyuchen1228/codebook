// include  Fast Power and BigInt Multiplication
// remember use the muti of BigInt in fp
void cal_rd(ll n, ll &r, ll &d) {
    r = 0;
    while(n % 2 == 0) {
        r++;
        n = n / 2;
    }
    d = n;
}
default_random_engine gen(time(NULL));
uniform_int_distribution<ll> unif(0, (ll)1e18);
bool is_prime(ll n) {
    if(n <= 1) return 0;
    if(n == 2 || n == 3) return 1;

    int times = 10;
    ll r, d;
    cal_rd(n - 1, r, d);
    while(times--) {
        ll a = unif(gen) % (n - 3) + 2;
        ll x = fp(a, d, n);
        if(x == 1 || x == n - 1)
            continue;
        bool probaly_prime = 0;
        for(int i = 0;i < r - 1;i++) {
            x = muti(x, x, n);
            if(x == n - 1) {
                probaly_prime = 1;
                break;
            }
        }
        if(probaly_prime)
            continue;
        return 0;
    }
    return 1;
}