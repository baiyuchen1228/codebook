// a_i=a_i-1+a_i-2+a_i-3+a_i-4+a_i-5+a_i-6
struct Matrix {
    ll mat[N][N];
};
Matrix zero() {
    Matrix zero;
    for(int i = 0;i < n;i++) {
        for(int j = 0;j < n;j++) zero.mat[i][j] = 0;
    }
    return zero;
}
Matrix init() {
    // According to Problem
    Matrix a = zero();
    for(int i = 0;i < n;i++) {
        a.mat[i][i + 1] = 1;
        a.mat[n - 1][i] = 1;
    }
    return a;
}
Matrix I() {
    Matrix I = zero();
    for(int i = 0;i < n;i++) I.mat[i][i] = 1;
    return I;
}
Matrix matrix_muti(Matrix x, Matrix y, ll p) {
    Matrix c = zero();
    for(int i = 0;i < n;i++) {
        for(int j = 0;j < n;j++) {
            for(int k = 0;k < n;k++) {
                c.mat[i][j] = (c.mat[i][j] + x.mat[i][k] * y.mat[k][j]) % p;
            }
        }
    }
    return c;
}
Matrix fp(Matrix x, ll y, ll p) {
    Matrix res = I();
    while(y) {
        if(y & 1) res = matrix_muti(x, res, MOD);
        y = y >> 1;
        x = matrix_muti(x, x, MOD);
    }
    return res;
}
ll solve(ll x) {
    Matrix res = fp(init(), x - 1, MOD);
    ll v[n] = {1, 2, 4, 8, 16, 32};
    ll ans = 0;
    for(int i = 0;i < n;i++)
        ans = (ans + res.mat[0][i] * v[i]) % MOD;
    return ans;
}