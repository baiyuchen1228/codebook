tuple<ll, ll, ll> exgcd(ll a, ll b) { // x * a + y * b = gcd(a, b)
    if(b == 0) return make_tuple(1, 0, a);
    ll x, y, z;
    tie(x, y, z) = exgcd(b, a % b);
    return make_tuple(y, x - (a / b) * y, z);
}
/* to calculate a / b = c (% MOD) if gcd(b, MOD) = 1 (a and b can mod MOD previously)
 * tie(x, y, z) = exgcd(b, MOD); x * b + y * MOD = x * b = gcd(b, MOD) = 1 => x = b^(-1)
 * a / b = a * x = c */