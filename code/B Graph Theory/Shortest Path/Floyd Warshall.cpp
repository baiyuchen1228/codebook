void add_edge(int a, int b, ll w) {
    dis[a][b] = min(dis[a][b], w);
    dis[b][a] = min(dis[b][a], w);
}
void init() {
    memset(dis, 0x3f, sizeof(dis));
    // input and add_edge
    for(int i = 0;i <= n;i++) dis[i][i] = 0;
}
void Floyd_Warshell() {
    for(int k = 1;k <= n;k++) {
        for(int i = 1;i <= n;i++) {
            for(int j = 1;j <= n;j++) {
                dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
            }
        }
    }
    // dis[a][b] is the shortest distance between a and b
}