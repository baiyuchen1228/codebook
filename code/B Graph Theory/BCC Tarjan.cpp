int dfn[N], low[N]; bool cut[N];
int link_comp[M]; bool used[M];
// If the comp only contains one edge
// , the edge is bridge
vector<pair<int, int>> adj[N]; // {neighbor, egdeIndex}
stack<int> sta;
int ord = 1, comp = 0;
void DFS(int x, int p = -1) {
    dfn[x] = low[x] = ord++;
    int child_cnt = 0;
    for(auto [v, i] : adj[x]) {
        if(v == p) continue;
        if(!dfn[v]) {
            child_cnt++;
            sta.push(i);
            used[i] = true;
            DFS(v, x);
            low[x] = min(low[v], low[x]);
            if((dfn[x] != 1 && dfn[x] <= low[v]) || (dfn[x] == 1 && child_cnt >= 2)) {
                cut[x] = true;
                int top_idx = -1;
                comp++;
                while(top_idx != i) {
                    link_comp[sta.top()] = comp;
                    top_idx = sta.top();
                    sta.pop();
                }
            }
        } else {
            if(!used[i]) {
                sta.push(i);
                used[i] = true;
            }
            low[x] = min(dfn[v], low[x]);
        }
    }

    if(dfn[x] == 1) {
        if(!sta.empty()) {
            comp++;
            while(!sta.empty()) {
                link_comp[sta.top()] = comp;
                sta.pop();
            }
        }
    }
}