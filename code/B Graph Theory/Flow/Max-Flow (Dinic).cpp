struct Edge {
    int to; ll cap; int rev;
    Edge(){} // important
    Edge(int _to, ll _cap, int _rev): to(_to), cap(_cap), rev(_rev) {}
};
vector<Edge> adj[N];
int level[N], iter[N];
void add_edge(int from, int to, ll cap) {
    adj[from].push_back(Edge(to, cap, (int)adj[to].size()));
    adj[to].push_back(Edge(from, 0, (int)adj[from].size() - 1));
}
int s, t; // the max-flow of s to v;
void BFS() {
    memset(level, -1, sizeof(level));
    level[s] = 0;
    queue<int> q; 
    q.push(s);
    while(!q.empty()) {
        int frt = q.front();
        q.pop();
        for(auto e : adj[frt]) {
            if(e.cap > 0 && level[e.to] == -1) {
                level[e.to] = level[frt] + 1;
                q.push(e.to);
            }
        }
    }
}
ll DFS(int now, ll flow) {
    if(now == t) return flow;
    for(int &i = iter[now];i < (int)adj[now].size();i++) {// & is important for update iter[now]
        Edge &e = adj[now][i];
        if(e.cap > 0 && level[e.to] == level[now] + 1) {
            ll ret = DFS(e.to, min(flow, e.cap));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
ll max_flow() {
    ll ret = 0;
    while(1) {
        BFS();
        if(level[t] == -1) break;
        memset(iter, 0, sizeof(iter));
        ll tmp;
        while((tmp = DFS(s, INF)) > 0)  {
            ret += tmp;
        }
    }
    return ret;
}
int main() {
    s = 1, t = n;
    // add all edge, add_edge(from, to, cap);
    ans = max_flow();
}