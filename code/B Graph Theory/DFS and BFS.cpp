void DFS(int x) {
   vis[x] = 1;
   for(auto i : adj[x]) {
       if(!vis[i]) DFS(i);
   }
}
// ------------------
while(!BFS.empty()) {
    int x = BFS.front();
    BFS.pop();
    for(auto i : adj[x]) {
        if(!vis[i]) {
            vis[i] = 1;
            dis[i] = dis[x] + 1;
            BFS.push(i);
            parent[i] = x;
        }
    }
}