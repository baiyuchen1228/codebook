priority_queue<pii, vector<pii>, greater<pii>> pq; // (weight, node)
vector<pii> adj[N]; // (neighbor, weight)
bool mark[N];
int n, m;
ll Prim() {
	pq.push(pii(0, 1));
	ll ans = 0;
	while(!pq.empty()) {
		int x = pq.top().second;
		int weight = pq.top().first;
		pq.pop();
		if(mark[x]) continue;
		mark[x] = true;
		ans += weight;
		for(auto [v, w] : adj[x]) {
			if(!mark[v]) {
				pq.push(pii(w, v));
			}
		}
	}
	for(int i = 1; i <= n; i++) {
		if(!mark[i]) return 0;
	}
	return ans;
}