// #include Union-find
int n, m;
ll Kruskal() {
    init(); // Union-Find init
    // edge[i] = {a, b, w}
    sort(edge, edge + m, [](Edge a, Edge b) { return a.w < b.w; }); 
    int idx = 0;
    ll ans = 0;
    for(int i = 0;i < n - 1;i++) {
        while(idx < m && find(edge[idx].a) == find(edge[idx].b))
            idx++;
        if(idx == m)
            return 0;
        unite(edge[idx].a,edge[idx].b);
        ans = ans + edge[idx].w;
    }
    return ans;
}